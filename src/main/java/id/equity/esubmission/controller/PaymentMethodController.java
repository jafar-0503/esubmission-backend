package id.equity.esubmission.controller;

import id.equity.esubmission.dto.PaymentMethod.CreatePaymentMethod;
import id.equity.esubmission.dto.PaymentMethod.PaymentMethodDto;
import id.equity.esubmission.model.PaymentMethod;
import id.equity.esubmission.service.PaymentMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/payment-methods")
public class PaymentMethodController {

    @Autowired
    private PaymentMethodService paymentMethodService;

    //get All PaymentMethod
    @GetMapping
    public ResponseEntity<List<PaymentMethod>> listPaymentMethod(){
        return paymentMethodService.listPaymentMethod();
    }

    //Get PaymentMethod By Id
    @GetMapping("{id}")
    public ResponseEntity<PaymentMethod> getpaymentMethodById(@PathVariable Long id) {
        return paymentMethodService.getpaymentMethodById(id);
    }

    //Post BaseRate
    @PostMapping
    public ResponseEntity<PaymentMethod> addPaymentMethod(@RequestBody CreatePaymentMethod newPaymentMethod) {
        return paymentMethodService.addPaymentMethod(newPaymentMethod);
    }

    //Put PaymentMethod
    @PutMapping("{id}")
    public ResponseEntity<PaymentMethod> editPaymentMethod(@RequestBody CreatePaymentMethod updatePaymentMethod, @PathVariable Long id) {
        return paymentMethodService.editPaymentMethod(updatePaymentMethod, id);
    }

    //Delete PaymentMethod
    @DeleteMapping("{id}")
    public ResponseEntity<PaymentMethod> deletePaymentMethod(@PathVariable Long id) {
        return paymentMethodService.deletePaymentMethod(id);
    }
}
