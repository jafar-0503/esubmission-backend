package id.equity.esubmission;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsubmissionBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsubmissionBackendApplication.class, args);
	}

}
