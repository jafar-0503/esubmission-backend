package id.equity.esubmission.dto.PaymentMethod;

import lombok.Data;

import javax.persistence.Column;

@Data
public class PaymentMethodDto {

    private Long id;
    private String paymentMethodCode;
    private String description;
    private boolean isActive;
}
